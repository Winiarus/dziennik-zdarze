﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace LogClient
{
    /// <summary>
    /// Klasa zarządzająca
    /// umożliwia określanie parametrów zgłaszanego zdarzenia oraz wysyłanie go do aplikacji agenckiej przez sieć TCP
    /// </summary>
    public partial class Form2 : Form
    {
        /// <summary>
        /// Przesyłany string z danymi
        /// </summary>
        string log_out;

        /// <summary>
        /// Nazwa użytkownika zgłaszającego zdarzenie
        /// </summary>
        static string user_name;

        /// <summary>
        /// Adres IP serwera na którym działa agent
        /// </summary>
        static string server_ip;

        /// <summary>
        /// Port na którym pracuje agent
        /// </summary>
        static string server_port;

        /// <summary>
        /// Initializes a new instance of the <see cref="Form2"/> class.
        /// </summary>
        /// <param name="temp_user_name">The temp_user_name.</param>
        /// <param name="temp_ip">The temp_ip.</param>
        /// <param name="temp_port">The temp_port.</param>
        public Form2(string temp_user_name, string temp_ip, string temp_port)
        {
            InitializeComponent();
            this.Text = "Zarządzanie wpisami";
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;
            timer1.Start();
            user_name = temp_user_name;
            server_ip = temp_ip;
            server_port = temp_port;
            label4.Text = user_name;
        }


        /// <summary>
        /// Metoda umożliwia przesłanie określonej wiadomości przez sieć TCP do aplikacji agenta
        /// </summary>
        /// <param name="message">wiadomość</param>
        private static void SendMessage(string message)
        {
            try
            {
                Int32 port = Int32.Parse(server_port);
                TcpClient client = new TcpClient(server_ip, port);
                Byte[] data = System.Text.Encoding.UTF8.GetBytes(message);
                NetworkStream stream = client.GetStream();
                stream.Write(data, 0, data.Length);
                data = new Byte[256];
                String responseData = String.Empty;
                Int32 bytes = stream.Read(data, 0, data.Length);
                responseData = System.Text.Encoding.UTF8.GetString(data, 0, bytes);
                if (responseData == "True")
                    MessageBox.Show("Wpis został dodany poprawnie!");
                else
                    MessageBox.Show("Wystąpił błąd, spróbuj ponownie!");
                stream.Close();
                client.Close();
            }
            catch (ArgumentNullException e) {}
            catch (SocketException e) {
                MessageBox.Show("Błąd połączenia z siecią! Zrestartuj aplikację! : " + e);
            }

            Console.Read();
        }

        /// <summary>
        /// Metoda realizowana po naciśnięciu przycisku "Wyślij Log"
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click(object sender, EventArgs e)
        {
            DateTime dateTime1 = DateTime.Now;
            if (Convert.ToString(comboBox2.SelectedItem) == "Alarm")
                log_out = "Alarm" + "#" + dateTime1.ToString() + "#" + user_name + "#" + comboBox1.SelectedItem + "#" + textBox1.Text;
            else if (Convert.ToString(comboBox2.SelectedItem) == "Zdarzenie")
                log_out = "Zdarzenie" + "#" + dateTime1.ToString() + "#" + user_name + "#" + "" +"#" + textBox1.Text;

            // wyślij wiadomość do agenta
            SendMessage(log_out);
        }

        /// <summary>
        /// Odliczanie czasu w GUI - czas aktualny
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dateTime = DateTime.Now;
            this.label5.Text = dateTime.ToString();
        }

        /// <summary>
        /// Wyłączenie aplikacji
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}
