﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace LogClient
{   

    /// <summary>
    /// Klasa Logowania do klienta
    /// </summary>
    public partial class Form1 : Form
    {

        /// <summary>
        /// Konstruktor
        /// Initializes a new instance of the <see cref="Form1"/> class.
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            this.Text = "Logowanie"; // nazwa okna
            user_name.SelectedIndex = 0;
        }

        /// <summary>
        /// Metoda sprawdza czy podane przez użytkownika a GUI dane połączeniowe do agenta działają poprawnie
        /// </summary>
        /// <param name="tmp_server">The tmp_server.</param>
        /// <param name="tmp_port">The tmp_port.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        private static bool checkConnection(string tmp_server, string tmp_port)
        {
            bool check = false;
            try
            {
                Int32 port = Int32.Parse(tmp_port);
                TcpClient client = new TcpClient(tmp_server, port);
                check = client.Connected;
                client.Close();
            }
            catch (SocketException e)
            {
                check = false;
                MessageBox.Show("Nie można połączyć, spróbuj ponownie! :" + e);
            }
            return check;
        }

        /// <summary>
        /// Metoda zdalnego logowania do aplikacji agenta.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (checkConnection(textBox1.Text, textBox2.Text))
            {
                if (Convert.ToString(user_name.SelectedItem) != "")
                {
                    this.Hide();
                    Form2 f2 = new Form2(Convert.ToString(user_name.SelectedItem), textBox1.Text, textBox2.Text);
                    f2.ShowDialog();
                    this.Close();
                }
            }
        }

        /// <summary>
        /// Zamykanie aplikacji przyciskiem "Wyjdź"
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
