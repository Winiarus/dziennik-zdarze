﻿namespace ZSTLogViewer
{
    partial class LogViewerApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.read = new System.Windows.Forms.CheckBox();
            this.type = new System.Windows.Forms.CheckedListBox();
            this.searchInDB = new System.Windows.Forms.Button();
            this.time_from = new System.Windows.Forms.DateTimePicker();
            this.time_to = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.source = new System.Windows.Forms.CheckedListBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.update = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // read
            // 
            this.read.AutoSize = true;
            this.read.Location = new System.Drawing.Point(539, 29);
            this.read.Name = "read";
            this.read.Size = new System.Drawing.Size(84, 17);
            this.read.TabIndex = 0;
            this.read.Text = "Przeczytane";
            this.read.UseVisualStyleBackColor = true;
            this.read.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // type
            // 
            this.type.FormattingEnabled = true;
            this.type.Items.AddRange(new object[] {
            "Warning",
            "Minor",
            "Major",
            "Critical",
            "Cleared",
            "Indeterminate"});
            this.type.Location = new System.Drawing.Point(377, 24);
            this.type.Name = "type";
            this.type.Size = new System.Drawing.Size(120, 94);
            this.type.TabIndex = 1;
            // 
            // searchInDB
            // 
            this.searchInDB.Location = new System.Drawing.Point(548, 61);
            this.searchInDB.Name = "searchInDB";
            this.searchInDB.Size = new System.Drawing.Size(75, 23);
            this.searchInDB.TabIndex = 3;
            this.searchInDB.Text = "Znajdź";
            this.searchInDB.UseVisualStyleBackColor = true;
            this.searchInDB.Click += new System.EventHandler(this.button1_Click);
            // 
            // time_from
            // 
            this.time_from.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.time_from.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.time_from.Location = new System.Drawing.Point(61, 72);
            this.time_from.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.time_from.Name = "time_from";
            this.time_from.Size = new System.Drawing.Size(151, 20);
            this.time_from.TabIndex = 6;
            this.time_from.Value = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            // 
            // time_to
            // 
            this.time_to.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.time_to.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.time_to.Location = new System.Drawing.Point(61, 98);
            this.time_to.MinDate = new System.DateTime(2014, 1, 1, 0, 0, 0, 0);
            this.time_to.Name = "time_to";
            this.time_to.Size = new System.Drawing.Size(151, 20);
            this.time_to.TabIndex = 7;
            this.time_to.Value = new System.DateTime(2016, 1, 1, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(67, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Data wystąpienia:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Od:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Do:";
            // 
            // source
            // 
            this.source.FormattingEnabled = true;
            this.source.Items.AddRange(new object[] {
            "Source0",
            "Source1",
            "Source2",
            "Source3",
            "Source4",
            "Source5",
            "Source6",
            "Source9"});
            this.source.Location = new System.Drawing.Point(236, 24);
            this.source.Name = "source";
            this.source.Size = new System.Drawing.Size(120, 94);
            this.source.TabIndex = 11;
            // 
            // dataGridView
            // 
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(13, 131);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.Size = new System.Drawing.Size(610, 322);
            this.dataGridView.TabIndex = 13;
            // 
            // update
            // 
            this.update.Location = new System.Drawing.Point(548, 90);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(75, 23);
            this.update.TabIndex = 14;
            this.update.Text = "Aktualizuj";
            this.update.UseVisualStyleBackColor = true;
            this.update.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Wszystkie",
            "Alarmy",
            "Zdarzenia"});
            this.comboBox1.Location = new System.Drawing.Point(91, 24);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 15;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Dziennik";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // LogViewerApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 465);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.update);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.source);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.time_to);
            this.Controls.Add(this.time_from);
            this.Controls.Add(this.searchInDB);
            this.Controls.Add(this.type);
            this.Controls.Add(this.read);
            this.Name = "LogViewerApp";
            this.Text = "LogViewer ZST";
            this.Load += new System.EventHandler(this.LogViewerApp_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox read;
        private System.Windows.Forms.CheckedListBox type;
        private System.Windows.Forms.Button searchInDB;
        private System.Windows.Forms.DateTimePicker time_from;
        private System.Windows.Forms.DateTimePicker time_to;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckedListBox source;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button update;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label4;
    }
}

