﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;

namespace ZSTLogViewer
{
    /// <summary>
    /// Klasa okienkowa, zapewniająca GUI aplikacji
    /// </summary>
    public partial class LogViewerApp : Form
    {
        /// <summary>
        /// Miejsce, w którym znajduje się plik z bazą danych MS Access
        /// </summary>
        private string whereDB = Directory.GetCurrentDirectory() + @"\DB.accdb";

        /// <summary>
        /// Zmienna włącza tryb testowy, developerski. Można na bieżąco obserwować wykonywane zapytania do bazy danych
        /// </summary>
        private bool Debug = false;

        /// <summary>
        /// Zmienna przechowuje informację, czy baza danych została już choć raz wczytana
        /// </summary>
        private bool LoadedDB = false;

        
        /// <summary>
        /// OleDbConnection - Dane niezbędne do definiowania połączenia z bazą danych
        /// </summary>
        OleDbConnection connection;
        /// <summary>
        /// OleDbDataAdapter
        /// </summary>
        OleDbDataAdapter da;
        /// <summary>
        /// OleDbCommand - Dane niezbędne do definiowania połączenia z bazą danych
        /// </summary>
        OleDbCommand cmd;
        /// <summary>
        /// DataSet - Dane niezbędne do definiowania połączenia z bazą danych
        /// </summary>
        DataSet ds = new DataSet();

        List<string> wantedTables = new List<string>();

        /// <summary>
        /// Zdefiniowane typy zdarzeń i odpowiadające im nazwy tabel bazy danych
        /// </summary>
        Dictionary<string, string> DefinedTablesKey = new Dictionary<string, string> {
            { "Alarmy", "LogRecord0" },
            { "Zdarzenia", "LogRecord1" }
        };
        
       
        /// <summary>
        /// Initializes a new instance of the <see cref="LogViewerApp"/> class.
        /// </summary>
        public LogViewerApp()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Prywatna metoda odpowiedzialna za ładowanie zapytań do bazy danych i parsowanie danych wynikowych
        /// </summary>
        private void appendContentFromDB()
        {
            connection = new OleDbConnection();
            connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + whereDB + ";Persist Security Info=False;";
            try
            {
                connection.Open();
                string query = generateSQLQuery();

                // debug
                if (Debug)
                    MessageBox.Show(query);

                da = new OleDbDataAdapter(query, connection);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView.DataSource = dt;

                if (!LoadedDB)
                {
                    DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
                    checkColumn.Name = "Delete";
                    checkColumn.HeaderText = "Usunąć?";
                    checkColumn.Width = 70;
                    checkColumn.ReadOnly = false;
                    checkColumn.FillWeight = 10;
                    dataGridView.Columns.Add(checkColumn);
                    LoadedDB = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>
        /// Metoda działa po kliknięciu przycisku "Znajdź"
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click(object sender, EventArgs e)
        {
            appendContentFromDB();
        }

        /// <summary>
        /// Metoda działa po kliknięciu przycisku "Aktualizuj"
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click_1(object sender, EventArgs e)
        {
            connection = new OleDbConnection();
            connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + whereDB + ";Persist Security Info=False;";
            List<string> Q = new List<string>();
            string datatable;
            try
            {
                connection.Open();
                foreach (DataGridViewRow row in dataGridView.Rows)
                {
                    if (row.Cells["Type"].Value == null || row.Cells["Type"].Value.ToString() == "") { datatable = "LogRecord1"; } else { datatable = "LogRecord0"; }
                    if (row.Cells["Delete"].Value != null)
                    {
                        cmd = new OleDbCommand("delete from [" + datatable + "] where [Time] = CDate('" + row.Cells["Time"].Value.ToString() + "')", connection);
                        cmd.ExecuteNonQuery();
                    }
                    else if (row.Cells["Read"].Value != null)
                    {
                        int read = Convert.ToInt32(row.Cells["Read"].Value.ToString().Equals("True"));
                        cmd = new OleDbCommand("update [" + datatable + "] set [Read] = " + read + " where [Time] = CDate('" + row.Cells["Time"].Value.ToString() + "')", connection);
                        cmd.ExecuteNonQuery();
                    }
                }
                connection.Close();
                MessageBox.Show("Zapisanie zmian powiodło się!", "Zmiany zapisano", MessageBoxButtons.OK, MessageBoxIcon.Information);
                appendContentFromDB();
            }
            catch (Exception eq)
            {
                MessageBox.Show(eq.ToString());
            }
        }

        /// <summary>
        /// Prywatna metoda generuje odpowiednio skonstruowane zapytanie SQL do bazy danych, na bazie wybranyh przez użytkownika opcji z GUI
        /// </summary>
        /// <returns>System.String.</returns>
        private string generateSQLQuery()
        {
            List<string> TablesQuery = new List<string>();

            foreach (string tbl in wantedTables)
            {
                List<string> A = new List<string>();
                List<string> qArray = new List<string>();
                string q = "select * from [" + tbl + "]";

                qArray.Add("[Time] >= CDate('" + time_from.Value.ToString("HH:mm:ss yyyy-MM-dd") + "')");
                qArray.Add("[Time] <= CDate('" + time_to.Value.ToString("HH:mm:ss yyyy-MM-dd") + "')");

                if (Convert.ToBoolean(read.Checked))
                {
                    qArray.Add("[Read] = True");
                }

                List<string> x0 = new List<string>();
                foreach (string x0i in source.CheckedItems.Cast<string>().ToList())
                {
                    x0.Add("[Source] = '" + x0i + "'");
                }

                List<string> x2 = new List<string>();
                foreach (string x2i in type.CheckedItems.Cast<string>().ToList())
                {
                    x2.Add("[Type] = '" + x2i + "'");
                }

                q += " where";
                if (qArray.Any())
                    A.Add(" " + string.Join(" and ", qArray.ToArray()));
                if (x0.Any())
                    A.Add(" " + string.Join(" or ", x0.ToArray()));
                if (x2.Any())
                    A.Add(" " + string.Join(" or ", x2.ToArray()));

                q += string.Join(" and", A.ToArray());
                TablesQuery.Add(q);
            }
            return string.Join(" union ", TablesQuery.ToArray());
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Wybór rodzajów zdarzeń, które chcemy przeglądać
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            wantedTables.Clear();
            string wybrane = comboBox1.SelectedItem.ToString();
            if (wybrane == "Wszystkie")
            {
                foreach (KeyValuePair<string,string> entry in DefinedTablesKey)
                {
                    wantedTables.Add(entry.Value);
                }
            }
            else
            {
                foreach (KeyValuePair<string, string> entry in DefinedTablesKey)
                {
                    if (entry.Key == wybrane)
                    {
                        wantedTables.Add(entry.Value);
                    }
                }
            }
        }

        private void LogViewerApp_Load(object sender, EventArgs e)
        {

        }

    }
}
