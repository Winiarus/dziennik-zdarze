﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZSTLogViewer
{
  
    /// <summary>
    /// Główna klasa aplikacji
    /// </summary>
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            // inicjalizacja okna aplikacji
            Application.Run(new LogViewerApp());
        }
    }
}
