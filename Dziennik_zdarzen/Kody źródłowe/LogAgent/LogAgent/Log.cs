﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.Net;
using System.Net.Sockets;
using System.IO;

/// <summary>
/// The classLogAgent namespace.
/// </summary>
namespace classLogAgent
{
    /// <summary>
    /// Klasa Log przechowuje wszelkie niezbędne parametry zgłaszanego przez klienta zdarzenia
    /// </summary>
    class Log
    {
        /// <summary>
        /// Zdefiniowane nazwy tabel bazy danych odnoszące się do konkretnych typów zdarzeń
        /// </summary>
        private Dictionary<string, string> DefinedTablesKey = new Dictionary<string, string> {
            { "Alarm", "LogRecord0" },
            { "Zdarzenie", "LogRecord1" }
        };

        /// <summary>
        /// Zmienna przechowująca tabelę bazy danych, do której zdarzenie ma być zapisane.
        /// </summary>
        private string LogTable;

        
        /// <summary>
        /// Zmienna przechowująca datę wystąpienia przesyłanego zdarzenia
        /// </summary>
        private string Date;

        
        /// <summary>
        /// Zmienna przechowująca typ przesyłanego zdarzenia
        /// </summary>
        private string Type;

        
        /// <summary>
        /// Zmienna przechowująca nazwę użytkownika zgłaszającego zdarzenie
        /// </summary>
        private string Source;

       
        /// <summary>
        /// Zmienna przechowująca treść zdarzenia
        /// </summary>
        private string Value;

 
        /// <summary>
        /// Initializes a new instance of the <see cref="Log"/> class.
        /// </summary>
        public Log() { }


        /// <summary>
        /// Initializes a new instance of the <see cref="Log"/> class.
        /// </summary>
        /// <param name="data">The data.</param>
        public Log(string data)
        {
            string[] dataTable = Regex.Split(data, "#");
            LogTable = selectTable(dataTable[0]);
            Date = dataTable[1];
            Source = dataTable[2];
            Type = dataTable[3];
            Value = dataTable[4];
        }

       
        /// <summary>
        /// Metoda zwraca ścieżkę dostępową do bazy danych. Powinna ona znajdować się w tym samym katalogu co uruchamiana aplikacja Agenta.
        /// </summary>
        /// <returns>ścieżka dostępowa do bazy danych.</returns>
        private static string getDirectory()
        {
            string path = System.Reflection.Assembly.GetExecutingAssembly().CodeBase + @"DB.accdb;";
            path = path.Replace("file:///", "");
            path = path.Replace(System.AppDomain.CurrentDomain.FriendlyName, "");
            return path;
        }

        
        /// <summary>
        /// Metoda konwertuje typ przesyłanego zdarzenia na odpowiadającą mu tabelę bazy danych.
        /// </summary>
        /// <param name="type">zmienna przechowująca typ zdarzenia</param>
        /// <returns>nazwa tabeli bazy danych dopasowana do typu zdarzenia</returns>
        private string selectTable(string type)
        {
            string wanted = "";
            foreach (KeyValuePair<string, string> entry in DefinedTablesKey)
            {
                if (entry.Key == type)
                {
                    wanted = entry.Value;
                    break;
                }
            }
            return wanted;
        }

        
        /// <summary>
        /// Metoda odpowiedzialna za zapis przesyłanego zdarzenia do bazy danych.
        /// </summary>
        public bool addToDB()
        {
             int affectedRows = 0;
             OleDbConnection connection = new OleDbConnection();
             connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + getDirectory() + ";Persist Security Info=False;";
             try
             {
                 connection.Open();
                 OleDbCommand command = new OleDbCommand();
                 command.Connection = connection;
                 string intoDataBase = "INSERT INTO " + this.LogTable + " ([Time], [Type], [Source], [Value]) VALUES ('" + this.Date + "', '" + this.Type + "', '" + this.Source + "', '" + this.Value + "')";
                 command.CommandText = intoDataBase;
                 affectedRows = command.ExecuteNonQuery();
                 connection.Close();
             }
             catch (Exception ex) {
                 Console.WriteLine(ex);
             }
                    if(affectedRows < 1)
                        return false;
                    else
                        return true;
        }
    }
}
