﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Net;
using System.Net.Sockets;
using System.IO;
using classLogAgent;

namespace LogAgent
{
    /// <summary>
    /// Główna klasa aplikacji Agenckiej
    /// </summary>
    class Program
    {
        /// <summary>
        /// Zmienna przechwuje tryb pracy programu.
        /// true - pracujemy lokalnie, wtedy adres IP agenta to 127.0.0.1
        /// false - aktualny adres IP widoczny z sieci. 
        /// </summary>
        private static bool workingLocal = false;

        /// <summary>
        /// Metoda ta pobiera z ustawień DNS systemu Windows aktulnie przypisany karcie sieciowej adres IP
        /// </summary>
        /// <returns>Lokalne IP</returns>
        static string GetIP()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                    break;
                }
            }
            return localIP;
        }

        
        /// <summary>
        /// Główna metoda programu Agenta
        /// </summary>
        /// <param name="args">The arguments.</param>
        static void Main(string[] args)
        {
            if (args[0] == null || args[0] == "")
            {
                Console.WriteLine("Brak numeru portu! Sposób uruchomienia: LogAgent.exe <PORT>");
                System.Environment.Exit(1);
            }
            else
            {
                Int32 port = Int32.Parse(args[0]);
                Console.WriteLine("Agent działa poprawnie!");
                Console.WriteLine("Parametry Serwera: " + GetIP() + ":" + port);
                Console.WriteLine("====================");
                IPAddress localAddr;
                TcpListener server = null;
                int i;

                try
                {
                    if (workingLocal)
                        localAddr = IPAddress.Parse("127.0.0.1");
                    else
                        localAddr = IPAddress.Parse(GetIP());

                    server = new TcpListener(localAddr, port);
                    server.Start();

                    Byte[] bytes = new Byte[256];
                    String data = null;

                    while (true)
                    {
                        TcpClient client = server.AcceptTcpClient();
                        NetworkStream stream = client.GetStream();
                        data = null;
                        while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            data = System.Text.Encoding.UTF8.GetString(bytes, 0, i);

                            Log log = new Log(data);
                            bool writtenToDB = log.addToDB();
                            if (writtenToDB)
                                Console.WriteLine("Dodaję wpis: " + data + " -> Dodano!");
                            else
                                Console.WriteLine("Dodaję wpis: " + data + " -> BŁĄD!");
                            byte[] msg = System.Text.Encoding.UTF8.GetBytes(writtenToDB.ToString());
                            stream.Write(msg, 0, msg.Length);
                        }
                        client.Close();
                    }
                }
                catch (SocketException e)
                {
                    Console.WriteLine("Błąd: " + e);
                }

                Console.Read();
                server.Stop();
            }
        }
    }
}
